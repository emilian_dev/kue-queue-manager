import fetch from 'node-fetch';
import jobsA from "./todo-list-3050.json";
import jobsB from "./todo-list-3051.json";
import jobsC from "./todo-list-3052.json";

const jobs ={
    3050: jobsA,
    3051: jobsB,
    3052: jobsC
}

const port = process.env.PORT
console.log("PORT", port);

jobs[port].forEach(job => {
    console.log("Deleting job ", job.id)
    fetch(`http://localhost:${port}/job/${job.id}`, {
        "method": "DELETE"
    }).catch(console.error);
});
