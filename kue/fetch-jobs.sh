port=$1
job_state=$2
now=`date +%Y-%m-%dT%H:%M:%S%z`

echo "PORT: $port | job_state: $job_state | now: $now"

curl "http://localhost:$port/jobs/$job_state/0..50000/desc" >"./data/$job_state-jobs-$port-$now.json"
cp "./data/$job_state-jobs-$port-$now.json" "./todo-list-$port.json"
